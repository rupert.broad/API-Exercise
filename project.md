# CS-API Project

Not being a software developer, writing an API from scratch is a pretty a tall order. Luckily someone already did most of the work for me. Based on series of posts from [Real Python](https://realpython.com/flask-connexion-rest-api/)

Additional changes did need to be made to be able to connect to the RDS backend instead of SQLlite and to meet the requirements of the project in terms of the csv data.

The app allows the successful creation and retrieval of records via the API and the web page. What doesn't seem to work is the deletion and update of records. It looks like there's an issue with the swagger definition.

[cs-api endoint](http://a6912e70332e611e9b68e0add9ac467b-1671356673.eu-west-1.elb.amazonaws.com:5000/)

## Not (yet) implemented

As noted elsewhere, the first next step would be to move the kubectl commands into the CI/CD pipeline so that the creation and configuration of the cluster can be fully automated.

The app itself needs to be deployed in an automated way either with CircleCI or CodePipeline/CodeBuild. As CircleCI doesn't natively integrate with Kubernetes, CDP/CDB with CodeDeploy might be a better choice.

There's no real monitoring at this stage, outside of what's provided with the k8s dashboard. AWS documentation doesn't provide many clues about how to tackle this issue. Monitoring the app, pod and containers can be achieved using Prometheus/Grafna, for exxample, but monitor the cluster through Cloudwatch appears to be a bit more challenging. The nodes can be monitored through Cloudwatch as per any other instnace.

At this point the app is deployed behind a load-balancer listening on port 5000. While this is accetable to get the app running it's not ideal for any DTAP environment. What's not clear is how to automate deployment of the ELB with a certificate on port 443 using kubectl. As AWS provides free SSL certs it would be preferable to use ACM to get the cert but again it's not clear to me yet how (or if it's even possible) to link k8s deployed ELB to the ACM certificate. It could be that an addtional AWS ALB is required to be able to use ACM but then how to automate the forwarding of traffic from the ALB to the ELB is a challenge as you need to determine the name of the ELB and when it changes. It would also be nice to secure the ingress security group rules for the ELB.

This would be a starting point for getting the infrastructure closer to a production ready state but is by no means a comprehensive list.

### Security

Apart from the use of insecure HTTP, the other major issue is the use of clear-text passwords in the environment variables for building the database connection string. Using AWS RDS & Terraform allowed creating a random password and storing it in SSM Parameters as a secure string. Implementing app depoyment via CI/CD would allow for the retreiving of the password automatically from SSM but doesn't necessarily solve the problem of clear-text passwords. It's a pretty obvious problem sO it would be surprising if there wasn't already a solution available.

## Non-gitlab repos

With the exception of the ECR terraform code, all other Terraform code is configured to be able to deploy a single codebase to multiple (DTAP) environments using CircleCI by leveraging Terraform Workspaces. For this project only the dev environment is deployed. Creating test and prod is simply a case of configuring the variables in tfvars and creating the appropriate PRs in Github.

Therefore for the cs-api-aws-* repos (except -ecr), the dev branch contains the most up to date code. UAT and master branches haven't been pushed as this would create additional test and prod resources.

### Why not Gitlab?

Coming from a non-software developer background it's been pretty challenging getting up to speed on the developer tools needed to write Infrastructure as code. From an outsiders viewpoint Github appears to be the defacto standard for dVCS and has integrations with the other tools we use - Slack, CodePipeline/CodeBuild, CircleCI, for example. Completing the exercise AND learning Gitlab was a bit of a stretch for one weekend.

### cs-api-aws-base

Terraform code to create VPC, security groups, ACM certificates, S3 buckets for logging and SNS notification topics.
[Terraform Registry module is used to speed up development time](https://registry.terraform.io/modules/terraform-aws-modules/vpc/aws/1.55.0)

[link to repo](https://github.com/rb-org/cs-api-aws-base)

### cs-api-aws-ec2

Terraform code to create EC2 instances. For this project only an SSH instance is needed and this is only really for the convenience of running manual tasks e.g. for setting up and populating the database.

[link to repo](https://github.com/rb-org/cs-api-aws-ec2)

### cs-api-aws-ecr

Terraform code for creating an ECR repository for the CS API images.

[link to repo](https://github.com/rb-org/cs-api-aws-ecr)

### cs-api-aws-eks

Terraform code for creating the EKS cluster and nodes. This code can be extend to automate the kubectl commands so that the creation of the cluster, deployment of the dashboard and deploymmnet of the node yaml can be performed by during creation as part of the CI/CD pipeline.

[link to repo](https://github.com/rb-org/cs-api-aws-eks)

### cs-api-aws-rds

Terraform code for creating the RDS MySQL database. For this project only a single instance cluster is created. Extending for multi-az high-availability is simply a case of specifiying the correct variables. [Terraform Registry module is used to speed up development time](https://registry.terraform.io/modules/terraform-aws-modules/rds/aws/1.22.0)

[link to repo](https://github.com/rb-org/cs-api-aws-rds)

### tfm-git

Terraform code to standardize the creation of Github repositories and basic configuration of CircleCI pipeline. CCI configuration will be expanded as/when the CCI api improves

[link to repo](https://github.com/rb-org/tfm-git)

### Terraform Modules

#### terraform-aws-rb-github

Module used by tfm-git

[link to repo](https://github.com/rb-org/terraform-aws-rb-github)

#### tfm-aws-mod-misc

Includes sub-modules for:

* AWS Certificate Manager certificate creation
* Routing SNS notifications to Slack

[link to repo](https://github.com/rb-org/tfm-aws-mod-misc)

#### tfm-aws-mod-sns-slack

Module used by tfm-aws-mod-misc (based on [Terraform regisry module](https://registry.terraform.io/modules/terraform-aws-modules/notify-slack/aws/1.11.0)) to create SNS topics and a lambda function that routes the messages to Slack. Original registry module has issues with zip file has which is why this was cloned and updated.

[link to repo](https://github.com/rb-org/tfm-aws-mod-sns-slack)

#### tfm-aws-mod-ec2-default

Module create a default EC2 instance including Cloudwatch montioring.

[link to repo](https://github.com/rb-org/tfm-aws-mod-ec2-default)